<?php

namespace Drupal\theme_switcher\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;

/**
 * Change theme based on the "something".
 *
 * @package Drupal\theme_switcher\Theme
 */
class ThemeswitcherNegotiator implements ThemeNegotiatorInterface {

  /**
   * This method determine if a switch should take place or not.
   */
  public function applies(RouteMatchInterface $route_match) {

    $current_path = \Drupal::service('path.current')->getPath();
    $path_alias = \Drupal::service('path.alias_manager')->getAliasByPath($current_path);

    // First, check if the "raw" path (e.g. `node/1`) matches.
    if ($current_path == '/test') {
      return TRUE;
    }
    // If not, check the path alias.
    elseif ($path_alias == '/test') {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * If the method about returns true, this method determine what theme to use.
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {

    $current_path = \Drupal::service('path.current')->getPath();
    $path_alias = \Drupal::service('path.alias_manager')->getAliasByPath($current_path);

    if ($current_path == '/test') {
      return 'seven';
    }
    elseif ($path_alias == '/test') {
      return 'seven';
    }
  }

}
